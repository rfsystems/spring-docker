package com.maplink.agendamento.service;

import com.maplink.agendamento.exception.ResourceNotFoundException;
import com.maplink.agendamento.models.Agendamento;
import com.maplink.agendamento.models.AgendamentoDTO;
import com.maplink.agendamento.models.Cliente;
import com.maplink.agendamento.models.Servico;
import com.maplink.agendamento.repository.AgendamentoRepository;
import com.maplink.agendamento.repository.ClienteRepository;
import com.maplink.agendamento.repository.ServicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AgendamentoService {

    @Autowired
    private AgendamentoRepository agendamentoRepository;
    @Autowired
    private ServicoRepository servicoRepository;
    @Autowired
    private ClienteRepository clienteRepository;

    public Agendamento remarcar(LocalDateTime data, Long idAgendamento){
        return agendamentoRepository
                .findById(idAgendamento)
                .orElseThrow(() -> new ResourceNotFoundException("Agendamento not found for this id :: " + idAgendamento));
    }

    public List<Agendamento> getAll(){
        return agendamentoRepository.findAll();
    }

    public Agendamento criar(AgendamentoDTO dto) {
        Agendamento agendamento = new Agendamento();
        Cliente cliente = clienteRepository.findById(dto.getCpf())
                .orElseThrow(() -> new ResourceNotFoundException("Cliente not found for this id :: " + dto.getCpf()));
        agendamento.setCliente(cliente);

        Servico servico = servicoRepository.findById(dto.getServicoId())
                .orElseThrow(() -> new ResourceNotFoundException("Servico not found for this id :: " + dto.getServicoId()));
        agendamento.setServico(servico);
        agendamento.setObservacao("Criação de Agendamento");
        return agendamentoRepository.save(agendamento);
    }
}
