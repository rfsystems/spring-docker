package com.maplink.agendamento.service;

import com.maplink.agendamento.exception.ResourceNotFoundException;
import com.maplink.agendamento.models.Cliente;
import com.maplink.agendamento.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    public Cliente save(Cliente cliente){
        return repository.save(cliente);
    }

    public Cliente update(Cliente cliente){
        Cliente cliente1 = repository.findById(cliente.getCpf())
                .orElseThrow(() -> new ResourceNotFoundException("Cliente not found for this id :: " + cliente.getCpf()));
        cliente1.setNome(cliente.getNome());
        return repository.save(cliente1);
    }

    public Cliente delete(String id){
        Cliente cliente = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Cliente not found for this id :: " + id));
        return cliente;
    }
    public List<Cliente> list(){
        return repository.findAll();
    }

}
