package com.maplink.agendamento.service;

import com.maplink.agendamento.exception.ResourceNotFoundException;
import com.maplink.agendamento.models.Servico;
import com.maplink.agendamento.repository.ServicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicoService {

    @Autowired
    private ServicoRepository repository;

    public Servico save(Servico servico){
        return repository.save(servico);
    }

    public Servico update(Servico servico){
        Servico servicoAtached = repository.findById(servico.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Servico not found for this id :: " + servico.getId()));
        servicoAtached.setDescricao(servico.getDescricao());
        servicoAtached.setValor(servico.getValor());
        return repository.save(servicoAtached);
    }

    public Servico delete(Long id){
        Servico servico = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Servico not found for this id :: " + id));
        repository.delete(servico);
        return servico;
    }

    public List<Servico> list(){
        return repository.findAll();
    }

}
