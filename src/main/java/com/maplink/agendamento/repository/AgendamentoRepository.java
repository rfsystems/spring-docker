package com.maplink.agendamento.repository;

import com.maplink.agendamento.models.Agendamento;
import com.maplink.agendamento.models.Servico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgendamentoRepository extends JpaRepository<Agendamento, Long> {
}
