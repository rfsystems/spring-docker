package com.maplink.agendamento.repository;

import com.maplink.agendamento.models.Servico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServicoRepository extends JpaRepository<Servico, Long> {
}
