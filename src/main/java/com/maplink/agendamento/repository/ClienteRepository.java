package com.maplink.agendamento.repository;

import com.maplink.agendamento.models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, String> {
}
