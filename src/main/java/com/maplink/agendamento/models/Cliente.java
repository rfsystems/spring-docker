package com.maplink.agendamento.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
public class Cliente {

    @Id @NonNull
    private String cpf;

    @NonNull
    private String nome;



}
