package com.maplink.agendamento.models;

import lombok.Data;
import org.springframework.lang.NonNull;

import java.time.LocalDateTime;

@Data
public class AgendamentoDTO {

    @NonNull
    private LocalDateTime dataHora;

    private String observacao;

    @NonNull
    private String cpf;

    @NonNull
    private Long servicoId;

}
