package com.maplink.agendamento.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class DateUtils {

    public static LocalDateTime dateFromLong(Long date){
        Instant instant = Instant.ofEpochSecond( date );
        ZoneId zoneId = ZoneId.of( "America/Sao_Paulo" );
        LocalDateTime ldt = LocalDateTime.ofInstant( instant , zoneId );
        return ldt;
    }

}
