package com.maplink.agendamento.resource;

import com.maplink.agendamento.models.Cliente;
import com.maplink.agendamento.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClienteResource {

    @Autowired
    private ClienteService service;

    @PostMapping(path = "/clientes")
    public ResponseEntity<Object> create(@RequestBody Cliente cliente){
        Cliente save = service.save(cliente);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @PutMapping(path = "/clientes")
    public ResponseEntity<Cliente> update(@RequestBody Cliente cliente){
        Cliente save = service.update(cliente);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @DeleteMapping(path = "/clientes/{cpf}")
    public ResponseEntity<Object> delete(@PathVariable String cpf){
        Cliente delete = service.delete(cpf);
        return new ResponseEntity<>(delete, HttpStatus.OK);
    }

    @GetMapping(path = "/clientes")
    public ResponseEntity<List<Cliente>> list(){
        return new ResponseEntity<>(service.list(), HttpStatus.OK);
    }

}
