package com.maplink.agendamento.resource;

import com.maplink.agendamento.models.Agendamento;
import com.maplink.agendamento.models.AgendamentoDTO;
import com.maplink.agendamento.service.AgendamentoService;
import com.maplink.agendamento.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

@RestController
public class AgendamentoResource {

    @Autowired
    private AgendamentoService service;

    @PostMapping(path = "/agendamentos")
    public ResponseEntity<Agendamento> agendar(@RequestBody AgendamentoDTO dto){
        Agendamento agendamento = service.criar(dto);
        return new ResponseEntity<>(agendamento, HttpStatus.OK);
    }

    @PutMapping(path = "/agendamentos/{idAgendamento}/date/{date}")
    public ResponseEntity<Agendamento> remarcar(@PathVariable(value = "date") Long data, @PathVariable(value = "idAgendamento") Long idAgendamento){
        LocalDateTime localDateTime = DateUtils.dateFromLong(data);
        Agendamento agendamento = service.remarcar(localDateTime, idAgendamento);
        return new ResponseEntity<>(agendamento, HttpStatus.OK);
    }

    @PutMapping(path = "/agendamentos")
    public ResponseEntity<List<Agendamento>> list(){
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @PutMapping(path = "/agendamentos/sorted")
    public ResponseEntity<Map<LocalDateTime, Map<Double, List<Agendamento>>>> listSorted(){
        List<Agendamento> serviceAll = service.getAll();

        Map<LocalDateTime, Map<Double, List<Agendamento>>> sorted = serviceAll.stream()
                .collect(groupingBy(Agendamento::getDataHora, groupingBy(Agendamento::getValorService)));
        return new ResponseEntity<>(sorted, HttpStatus.OK);

    }

}
