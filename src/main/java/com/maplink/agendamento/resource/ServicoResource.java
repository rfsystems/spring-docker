package com.maplink.agendamento.resource;

import com.maplink.agendamento.models.Servico;
import com.maplink.agendamento.service.ServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServicoResource {

    @Autowired
    private ServicoService service;

    @PostMapping(path = "/servicos")
    public ResponseEntity<Servico> create(@RequestBody Servico servico){
        Servico saved = service.save(servico);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @PutMapping(path = "/servicos")
    public ResponseEntity<Servico> update(@RequestBody Servico servico){
        Servico servicoUpdated = service.update(servico);
        return new ResponseEntity<>(servicoUpdated, HttpStatus.OK);
    }

    @DeleteMapping(path = "/servicos/{id}")
    public ResponseEntity<Servico> delete(@PathVariable Long id){
        Servico servicoDeleted = service.delete(id);
        return new ResponseEntity<>(servicoDeleted, HttpStatus.OK);
    }

    @GetMapping(path = "/servicos")
    public ResponseEntity<List<Servico>> list(){
        return new ResponseEntity<>(service.list(), HttpStatus.OK);
    }

}
