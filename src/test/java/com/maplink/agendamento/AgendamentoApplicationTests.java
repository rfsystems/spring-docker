package com.maplink.agendamento;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.*;


class AgendamentoApplicationTests {

	@Test
	void contextLoads() {

		LocalDateTime now = LocalDateTime.now();
		long l = now.toEpochSecond(ZoneOffset.UTC);
		System.out.println(l);

		Instant instant = Instant.ofEpochSecond( 1635171350L );
		ZoneId zoneId = ZoneId.of( "America/Sao_Paulo" );
		LocalDateTime zdt = LocalDateTime.ofInstant( instant , zoneId );
		System.out.println(zdt);


	}

}
