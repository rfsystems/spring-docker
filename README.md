# Getting Started

### Subindo o projeto

Você precisará ter instalado 

 docker, docker-compose

Primeiramente build sua imagem com o comando abaixo:

 - docker build -t spring/maplink-agendamento .

Com a imagem buildada, podemos subir o docker-compose, nele temos o servico da aplicação e o banco Postgres
 - docker-compose up

Com o projeto up, podemos acessar os recursos da API através do link do swagger

 - http://localhost:8080/swagger-ui.html#/

Nesse link vc encontrará todos os serviçoes de cadastro e suas respectivas consultas.


Opcional

Você pode subir a imagem em um container se quiser, mas ela precisa do banco postgres

 - docker run -p 8080:8080 spring/maplink-agendamento

Se optar por esse caminho, precisa alterar o host do banco para localhost, pois ele está mapeado para encontrar o servico docker de banco